package com.grishberg.obmenogram;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.grishberg.obmenogram.data.service.ApiConst;
import com.grishberg.obmenogram.framework.bitmap.BitmapLoadingProcessor;
import com.grishberg.obmenogram.injection.AppComponent;
import com.grishberg.obmenogram.injection.DaggerAppComponent;
import com.grishberg.obmenogram.injection.RestModule;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

/**
 * Created by g on 07.08.15.
 */
public class App extends Application {
    private static final String SHPREF_KEY_ACCESS_CODE = "keyAccessCode";
    private static final String SHPREF_KEY_ACCESS_TOKEN = "keyAccessToken";
    private static final String SHPREF_KEY_USER_NAME = "keyUserName";
    private static final String SHPREF_KEY_USER_PICTURE = "keyUserPicture";
    private static final String SHARED_PREFERENCES_KEY = "com.grishberg.obmenogram";
    private static App instance;
    private static Context appContext;
    protected static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        appContext = getApplicationContext();
        initImageLoader();
        initComponents(DaggerAppComponent
                .builder()
                //.appModule(new AppModule(this))
                .restModule(new RestModule(ApiConst.Api.END_POINT))
                .build()
        );
    }
    public void initComponents(AppComponent appComponent) {
        this.appComponent = appComponent;
    }


    private void initImageLoader() {
        // UNIVERSAL IMAGE LOADER SETUP
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisk(true).cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .showImageOnLoading(R.drawable.empty_bg)
                //.displayer(new FadeInBitmapDisplayer(300))
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .diskCacheExtraOptions(1080, 1080, new BitmapLoadingProcessor())
                .memoryCache(new WeakMemoryCache())
                .diskCacheSize(100 * 1024 * 1024).build();

        ImageLoader.getInstance().init(config);
        // END - UNIVERSAL IMAGE LOADER SETUP
    }

    public static App getInstance() {
        return instance;
    }

    public static Context getAppContext() {
        return appContext;
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    private static SharedPreferences getPreferences(int mode) {
        return getAppContext().getSharedPreferences(SHARED_PREFERENCES_KEY, mode);
    }

    public static void setUserName(String accessCode) {
        SharedPreferences prefs = getPreferences(MODE_PRIVATE);
        prefs.edit()
                .putString(SHPREF_KEY_USER_NAME, accessCode)
                .apply();
    }

    public static String getUserName() {
        return getPreferences(MODE_PRIVATE).getString(SHPREF_KEY_USER_NAME, null);
    }

    public static void setAccessToken(String accessCode) {
        SharedPreferences prefs = getPreferences(MODE_PRIVATE);
        prefs.edit()
                .putString(SHPREF_KEY_ACCESS_TOKEN, accessCode)
                .apply();
    }

    public static String getAccessToken() {
        return getPreferences(MODE_PRIVATE).getString(SHPREF_KEY_ACCESS_TOKEN, null);
    }

    public static void setUserPicture(String pictureUrl) {
        SharedPreferences prefs = getPreferences(MODE_PRIVATE);
        prefs.edit()
                .putString(SHPREF_KEY_USER_PICTURE, pictureUrl)
                .apply();
    }

    public static String getUserPicture() {
        return getPreferences(MODE_PRIVATE).getString(SHPREF_KEY_USER_PICTURE, null);
    }
}
