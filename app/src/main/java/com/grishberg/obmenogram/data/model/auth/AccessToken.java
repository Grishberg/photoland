package com.grishberg.obmenogram.data.model.auth;

import com.google.gson.annotations.SerializedName;
import com.grishberg.obmenogram.data.service.ApiConst;

public class AccessToken {
	@SerializedName(ApiConst.Fields.ACCESS_TOKEN)
	public  String accessToken;
	@SerializedName(ApiConst.Fields.USER)
	public User user;
}
