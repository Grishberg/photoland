package com.grishberg.obmenogram.data.service;

import com.grishberg.obmenogram.data.model.UsersFeedResponse;
import com.grishberg.obmenogram.data.model.auth.AccessToken;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.Call;

/**
 * Created by g on 09.08.15.
 */
public interface Api {
    /*
    @FormUrlEncoded
    @POST("/user/edit")
    User updateUser(@Field("first_name") String first, @Field("last_name") String last);
*/
    @GET(ApiConst.Api.USERS_SELF_FEED)
    Call<UsersFeedResponse> getUsersFeed(@Query(ApiConst.Fields.ACCESS_TOKEN) String accessToken
            , @Query(ApiConst.Fields.COUNT) int count);

    @GET(ApiConst.Api.USERS_SELF_FEED)
    Call<UsersFeedResponse> getUsersNextFeed(@Query(ApiConst.Fields.ACCESS_TOKEN) String accessToken
                    , @Query(ApiConst.Fields.MAX_ID) String nextMaxId
                    , @Query(ApiConst.Fields.COUNT) int count);

    // auth
    @FormUrlEncoded
    @POST(ApiConst.Api.AUTH_REQUEST_ACCESS_TOKEN)
    Call<AccessToken> requestAccessToken(@Field(ApiConst.Auth.CLIENT_ID) String clientId
            , @Field(ApiConst.Auth.CLIENT_SECRET) String clientSecret
            , @Field(ApiConst.Auth.GRANT_TYPE) String grandType
            , @Field(ApiConst.Auth.REDIRECT_URI) String redirectUri
            , @Field(ApiConst.Auth.CODE) String code);

}
