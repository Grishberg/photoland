package com.grishberg.obmenogram.data.model;

import com.google.gson.annotations.SerializedName;
import com.grishberg.obmenogram.data.service.ApiConst;

public class From {
	@SerializedName(ApiConst.Fields.USERNAME)
	public String username;
	@SerializedName(ApiConst.Fields.ID)
	public String id;
	@SerializedName(ApiConst.Fields.PROFILE_PICTURE)
	public String profilePicture;
	@SerializedName(ApiConst.Fields.FULL_NAME)
	public String fullName;
}
