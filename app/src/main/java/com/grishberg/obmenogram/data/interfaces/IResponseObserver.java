package com.grishberg.obmenogram.data.interfaces;

/**
 * Created by g on 10.08.15.
 */
public interface IResponseObserver {
    void onDone(boolean response);
}
