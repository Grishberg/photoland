package com.grishberg.obmenogram.data.interfaces;

/**
 * Created by g on 13.09.15.
 */
public interface IAuthCompleteObserver {
    void onAuth(boolean isSuccess);
}
