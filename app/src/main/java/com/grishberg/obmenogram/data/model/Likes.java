package com.grishberg.obmenogram.data.model;

import com.google.gson.annotations.SerializedName;
import com.grishberg.obmenogram.data.service.ApiConst;

import java.util.List;

public class Likes {
	@SerializedName(ApiConst.Fields.DATA)
	public List<Data> data;
	@SerializedName(ApiConst.Fields.COUNT)
	public int count;
}
