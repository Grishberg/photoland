package com.grishberg.obmenogram.data.service;

/**
 * Created by g on 09.08.15.
 */
public class ApiConst {
    private ApiConst() {
    }

    public final class Api {
        private Api() {
        }

        public static final String END_POINT = "https://api.instagram.com";
        public static final String USERS_SELF_FEED = "/v1/users/self/media/recent";
        public static final String AUTH_REQUEST_CODE = "/oauth/authorize";
        public static final String AUTH_REQUEST_ACCESS_TOKEN = "/oauth/access_token";

        public static final String REDIRECT_URI = "http://vk.com/grishberg";
        public static final String AUTH_URL = "https://api.instagram.com/oauth/authorize/";
        public static final String CLIENT_ID = "d6dc6f82f4074117852deaa755ac2daa";
        public static final String CLIENT_SECRET = "212b9ee8e0e14b7cb78a8e5f77acf973";

    }

    public final class Methods{
        private Methods(){}

        public static final String USERS_SELF_FEED = "/v1/users/self/feed";
        public static final String AUTH_REQUEST_CODE = "/oauth/authorize";
        public static final String AUTH_REQUEST_ACCESS_TOKEN = "/oauth/access_token";
        public static final String AUTH_URL = "https://api.instagram.com/oauth/authorize/";
    }

    public final class Fields {
        private Fields() {
        }
        public static final String ACCESS_TOKEN = "access_token";
        public static final String CLIENT_ID= "client_id";
        public static final String CLIENT_SECRET= "client_secret";
        public static final String REDIRECT_URI= "redirect_uri";
        public static final String RESPONSE_TYPE= "response_type";
        public static final String NEXT_URL = "next_url";
        public static final String NEXT_MAX_ID = "next_max_id";
        public static final String MAX_ID = "max_id";
        public static final String PAGINATION = "pagination";
        public static final String META = "meta";
        public static final String CODE = "code";
        public static final String LONGITUDE = "longitude";
        public static final String LATITUDE = "latitude";
        public static final String WIDTH = "width";
        public static final String URL = "url";
        public static final String HEIGHT = "height";
        public static final String LOW_RESOLUTION = "low_resolution";
        public static final String STANDARD_RESOLUTION = "standard_resolution";
        public static final String THUMBNAIL = "thumbnail";
        public static final String DATA = "data";
        public static final String COUNT = "count";
        public static final String FULL_NAME = "full_name";
        public static final String USERNAME = "username";
        public static final String PROFILE_PICTURE = "profile_picture";
        public static final String TEXT = "text";
        public static final String FROM = "from";
        public static final String LOCATION = "location";
        public static final String USER = "user";
        public static final String TAGS = "tags";
        public static final String CREATED_TIME = "created_time";
        public static final String ID = "id";
        public static final String TYPE = "type";
        public static final String IMAGES = "images";
        public static final String LIKES = "likes";
        public static final String COMMENTS = "comments";
        public static final String CAPTION = "caption";
        public static final String USER_HAS_LIKED = "user_has_liked";
        public static final String USERS_IN_PHOTO = "users_in_photo";
        public static final String LINK = "link";
        public static final String ATTRIBUTION = "attribution";
        public static final String FILTER = "filter";
        public static final String X = "x";
        public static final String Y = "y";
        public static final String POSITION = "position";
    }

    public final class Auth{
        private Auth(){}
        public static final String ID = "id";
        public static final String GRANT_TYPE= "grant_type";
        public static final String CLIENT_ID = "client_id";
        public static final String CLIENT_SECRET = "client_secret";
        public static final String AUTHORIZATION_CODE = "authorization_code";
        public static final String REDIRECT_URI = "redirect_uri";
        public static final String CODE = "code";

    }
}
