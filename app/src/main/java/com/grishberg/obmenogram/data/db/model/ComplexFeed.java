package com.grishberg.obmenogram.data.db.model;

import android.database.Cursor;

import com.grishberg.obmenogram.data.db.DbHelper;

/**
 * Created by g on 23.08.15.
 */
public class ComplexFeed {
    //FEED
    private long id;
    private String nextAccessToken;
    private String nextMaxId;
    private long createdTime;
    //IMAGE
    private long imageId;
    private String thumbnail;
    private int thumbnailWidth;
    private int thumbnailHeight;
    private String standardResolution;
    private int standardWidth;
    private int standardHeight;
    private String caption;
    //USER
    private String userId;
    private String username;
    private String profilepicture;
    private String fullname;

    public ComplexFeed(long id
            , String nextAccessToken
            , String nextMaxId
            , long createdTime

            , long imageId
            , String thumbnail
            , int thumbnailWidth
            , int thumbnailHeight
            , String standardResolution
            , int standardWidth
            , int standardHeight
            , String caption

            , String userId
            , String username
            , String fullname
            , String profilepicture ) {
        this.id = id;
        this.imageId = imageId;
        this.nextAccessToken = nextAccessToken;
        this.nextMaxId = nextMaxId;
        this.createdTime = createdTime;

        this.thumbnail = thumbnail;
        this.thumbnailWidth = thumbnailWidth;
        this.thumbnailHeight = thumbnailHeight;
        this.standardResolution = standardResolution;
        this.standardWidth = standardWidth;
        this.standardHeight = standardHeight;
        this.caption = caption;

        this.userId = userId;
        this.username = username;
        this.fullname = fullname;
        this.profilepicture = profilepicture;
    }

    public static ComplexFeed fromCursor(Cursor c){
        int idColId = c.getColumnIndex(DbHelper.COLUMN_ID);
        int nextAccessTokenId = c.getColumnIndex(DbHelper.FEED_NEXT_ACCESS_TOKEN);
        int nextMaxIdId = c.getColumnIndex(DbHelper.FEED_NEXT_MAX_ID);
        int createdTimeId = c.getColumnIndex(DbHelper.FEED_CREATED_TIME);

        int imageIdId = c.getColumnIndex(DbHelper.FEED_IMAGE_ID);
        int thumbnailId = c.getColumnIndex(DbHelper.IMAGE_THUMBNAIL);
        int thumbnailWidthId = c.getColumnIndex(DbHelper.IMAGE_THUMBNAIL_WIDTH);
        int thumbnailHeightId = c.getColumnIndex(DbHelper.IMAGE_THUMBNAIL_HEIGHT);
        int standardResolutionId = c.getColumnIndex(DbHelper.IMAGE_STANDARD_RESOLUTION);
        int standardWidthId = c.getColumnIndex(DbHelper.IMAGE_STANDARD_WIDTH);
        int standardHeightId = c.getColumnIndex(DbHelper.IMAGE_STANDARD_HEIGHT);
        int captionId = c.getColumnIndex(DbHelper.IMAGE_CAPTION);

        int userIdId = c.getColumnIndex(DbHelper.IMAGE_USER_ID);
        int usernameId = c.getColumnIndex(DbHelper.USERS_USERNAME);
        int fullnameId = c.getColumnIndex(DbHelper.USERS_FULL_NAME);
        int profilepictureId = c.getColumnIndex(DbHelper.USERS_PROFILE_PICTURE);

        return new ComplexFeed(
                c.getLong(idColId)
                , c.getString(nextAccessTokenId)
                , c.getString(nextMaxIdId)
                , c.getLong(createdTimeId)

                , c.getLong(imageIdId)
                , c.getString(thumbnailId)
                , c.getInt(thumbnailWidthId)
                , c.getInt(thumbnailHeightId)
                , c.getString(standardResolutionId)
                , c.getInt(standardWidthId)
                , c.getInt(standardHeightId)
                , c.getString(captionId)

                , c.getString(userIdId)
                , c.getString(usernameId)
                , c.getString(fullnameId)
                , c.getString(profilepictureId)
        );
    }

    public long getId() {
        return id;
    }

    public long getImageId() {
        return imageId;
    }

    public String getNextAccessToken() {
        return nextAccessToken;
    }
    public String getNextMaxId() {
        return nextMaxId;
    }

    public long getCreatedTime() {
        return createdTime;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public int getThumbnailWidth() {
        return thumbnailWidth;
    }

    public int getThumbnailHeight() {
        return thumbnailHeight;
    }

    public String getStandardResolution() {
        return standardResolution;
    }

    public int getStandardWidth() {
        return standardWidth;
    }

    public int getStandardHeight() {
        return standardHeight;
    }

    public String getCaption() {
        return caption;
    }

    public String getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getProfilepicture() {
        return profilepicture;
    }

    public String getFullname() {
        return fullname;
    }
}
