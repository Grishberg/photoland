package com.grishberg.obmenogram.data.service;

import android.content.Intent;
import android.os.*;
import android.util.Log;
import android.util.SparseArray;

import com.grishberg.obmenogram.App;
import com.grishberg.obmenogram.data.interfaces.IResponseObserver;
import com.grishberg.obmenogram.data.service.async_workers.AuthWorker;
import com.grishberg.obmenogram.data.service.async_workers.FeedRunnable;
import com.grishberg.obmenogram.data.service.async_workers.ImageLoaderRunnable;
import com.grishberg.obmenogram.framework.IApiService;
import com.grishberg.obmenogram.framework.IImageLoaderObserver;
import com.grishberg.obmenogram.framework.service.BaseThreadPoolService;

import java.util.concurrent.Future;

import javax.inject.Inject;

public class ApiService extends BaseThreadPoolService implements
        AuthWorker.IOnAuthListener
        , IResponseObserver
        , IImageLoaderObserver
        , IApiService {
    private static final String TAG = ApiService.class.getSimpleName();
    private static final String FEED_SCREEN = "feed_screen";
    private static final String IMAGE_DETAIL_SCREEN = "imageDetailScreen";
    /*
        Client ID 	d6dc6f82f4074117852deaa755ac2daa
        Client Secret 	212b9ee8e0e14b7cb78a8e5f77acf973
        Website URL 	http://vk.com/grishberg
        Redirect URI 	http://vk.com/grishberg
        Support Email 	grishberg@ya.ru
    */
    //private Object mBusReceiver = new Object()
    @Inject
    Api mApiService;
    private ApiServiceBinder mBinder = new ApiServiceBinder();

    public ApiService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate service");
        App.getAppComponent().inject(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onGetFeed(String token, String nextMaxId) {
        Log.d(TAG, " on get feed: token=" + token);
        FeedRunnable runnable = new FeedRunnable(1, mApiService, this, this, token, nextMaxId);
        Future future = mExecutor.submit(runnable);
        SparseArray<Future> queue = mTaskQueue.get(FEED_SCREEN);
        if (queue == null) {
            queue = new SparseArray<>(CORE_POOL_SIZE);
            mTaskQueue.put(FEED_SCREEN, queue);
        }
        queue.put(0, future);
    }

    @Override
    public void doAuthRequest(String code) {
        new AuthWorker(mApiService, this, code).execute();
    }

    @Override
    public void loadImage(long id, String url) {
        Future future = mExecutor.submit(
                new ImageLoaderRunnable(2, this, url, id));
        SparseArray<Future> queue = mTaskQueue.get(FEED_SCREEN);
        if (queue == null) {
            queue = new SparseArray<>(CORE_POOL_SIZE);
            mTaskQueue.put(FEED_SCREEN, queue);
        }
        queue.put((int) id, future);
    }

    @Override
    public void onResponseFeed(int count) {
        sendFeedMessage();
    }

    @Override
    public void onDone(boolean response) {
        Log.d(TAG, "on Done");
    }

    @Override
    public void onImageLoaded(long id, String uri) {
        onTaskDone(FEED_SCREEN, (int) id);
        Log.d(TAG, "image loaded id=" + id + " url=" + uri);
        sendImageLoadedMessage(id);
    }

    // auth received
    @Override
    public void onAuth(String accessToken, String userName, String userPicture) {
        sendAuthMessage(accessToken, userName, userPicture);
    }

    @Override
    public void onAuthFail() {

    }

    @Override
    protected IBinder getBinder() {
        return mBinder;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mExecutor.shutdown();
        cancelAll();
        Log.d(TAG, "on destroy service");
    }

    // service container for Activity
    public class ApiServiceBinder extends Binder {
        public IApiService getService() {
            return ApiService.this;
        }
    }
}
