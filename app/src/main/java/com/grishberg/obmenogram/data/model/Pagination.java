package com.grishberg.obmenogram.data.model;

import com.google.gson.annotations.SerializedName;
import com.grishberg.obmenogram.data.service.ApiConst;

/**
 * Created by g on 09.08.15.
 */
public class Pagination {
    @SerializedName(ApiConst.Fields.NEXT_URL)
    public String nextUrl;

    @SerializedName(ApiConst.Fields.PAGINATION)
    public String nextMaxId;
}
