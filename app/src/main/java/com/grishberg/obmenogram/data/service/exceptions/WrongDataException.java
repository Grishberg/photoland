package com.grishberg.obmenogram.data.service.exceptions;

/**
 * Created by grishberg on 29.06.16.
 */
public class WrongDataException extends Throwable {
    private static final String TAG = WrongDataException.class.getSimpleName();

    public WrongDataException(String detailMessage) {
        super(detailMessage);
    }
}
