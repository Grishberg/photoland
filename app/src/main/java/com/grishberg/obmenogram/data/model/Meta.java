package com.grishberg.obmenogram.data.model;

import com.google.gson.annotations.SerializedName;
import com.grishberg.obmenogram.data.service.ApiConst;

/**
 * Created by g on 09.08.15.
 */
public class Meta {
    @SerializedName(ApiConst.Fields.CODE)
    public String code;
}
