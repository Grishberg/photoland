package com.grishberg.obmenogram.data.db.model;

import com.grishberg.obmenogram.data.db.DbHelper;

import android.content.ContentValues;
import android.database.Cursor;

public class Feed {
    private long id;
    private String feedId;
    private long imageId;
    private String nextAccessToken;
    private String nextmaxid;
    private long createdTime;
    private String userName;
    private String userFullName;
    private String userPic;
    private String caption;
    private String imageUrl;
    private int imageWidth;
    private int imageHeight;

    public Feed(long id
            , String feedId
            , long imageId
            , String nextAccessToken
            , String nextmaxid
            , long createdTime

            , String userName
            , String userFullName
            , String userPic

            , String caption
            , String imageUrl
            , int imageWidth
            , int imageHeight
    ) {
        this.id = id;
        this.feedId = feedId;
        this.imageId = imageId;
        this.nextAccessToken = nextAccessToken;
        this.nextmaxid = nextmaxid;
        this.createdTime = createdTime;

        this.userName = userName;
        this.userFullName = userFullName;
        this.userPic = userPic;

        this.caption = caption;
        this.imageWidth = imageWidth;
        this.imageHeight = imageHeight;
        this.imageUrl = imageUrl;
    }

    public static Feed fromCursor(Cursor c) {
        int idColId = c.getColumnIndex(DbHelper.COLUMN_ID);
        int feedIdId = c.getColumnIndex(DbHelper.FEED_FEED_ID);
        int imageIdId = c.getColumnIndex(DbHelper.FEED_IMAGE_ID);
        int nexturlId = c.getColumnIndex(DbHelper.FEED_NEXT_ACCESS_TOKEN);
        int nextmaxidId = c.getColumnIndex(DbHelper.FEED_NEXT_MAX_ID);
        int createdTimeId = c.getColumnIndex(DbHelper.FEED_CREATED_TIME);
        int imageUrlId = c.getColumnIndex(DbHelper.FEED_IMAGE_URL);
        int imageWidthId = c.getColumnIndex(DbHelper.FEED_IMAGE_WIDTH);
        int imageHeightId = c.getColumnIndex(DbHelper.FEED_IMAGE_HEIGHT);
        int captionId = c.getColumnIndex(DbHelper.FEED_CAPTION);

        int userNameId = c.getColumnIndex(DbHelper.FEED_USERNAME);
        int userFullNameId = c.getColumnIndex(DbHelper.FEED_FULL_NAME);
        int userPicId = c.getColumnIndex(DbHelper.FEED_PROFILE_PICTURE);
        return new Feed(
                c.getLong(idColId)
                , c.getString(feedIdId)
                , c.getLong(imageIdId)
                , c.getString(nexturlId)
                , c.getString(nextmaxidId)
                , c.getLong(createdTimeId)

                , c.getString(userNameId)
                , c.getString(userFullNameId)
                , c.getString(userPicId)

                , c.getString(captionId)
                , c.getString(imageUrlId)
                , c.getInt(imageWidthId)
                , c.getInt(imageHeightId));
    }

    public ContentValues buildContentValues() {
        ContentValues cv = new ContentValues();
        if (id >= 0) {
            cv.put(DbHelper.COLUMN_ID, id);
        }
        cv.put(DbHelper.FEED_FEED_ID, feedId);
        cv.put(DbHelper.FEED_IMAGE_ID, imageId);
        cv.put(DbHelper.FEED_NEXT_ACCESS_TOKEN, nextAccessToken);
        cv.put(DbHelper.FEED_NEXT_MAX_ID, nextmaxid);
        cv.put(DbHelper.FEED_CREATED_TIME, createdTime);
        cv.put(DbHelper.FEED_IMAGE_URL, imageUrl);
        cv.put(DbHelper.FEED_IMAGE_WIDTH, imageWidth);
        cv.put(DbHelper.FEED_IMAGE_HEIGHT, imageHeight);
        cv.put(DbHelper.FEED_CAPTION, caption);
        cv.put(DbHelper.FEED_USERNAME, userName);
        cv.put(DbHelper.FEED_FULL_NAME, userFullName);
        cv.put(DbHelper.FEED_PROFILE_PICTURE, userPic);
        return cv;
    }

    //------------- getters ------------

    public String getUserName() {
        return userName;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public String getUserPic() {
        return userPic;
    }

    public String getCaption() {
        return caption;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public int getImageWidth() {
        return imageWidth;
    }

    public int getImageHeight() {
        return imageHeight;
    }

    public String getFeedId() {
        return feedId;
    }

    public long getImageId() {
        return imageId;
    }

    public String getNextAccessToken() {
        return nextAccessToken;
    }

    public String getNextmaxid() {
        return nextmaxid;
    }

    public long getCreatedTime() {
        return createdTime;
    }

    public long getId() {
        return id;
    }
}
