package com.grishberg.obmenogram.data.model;

import com.google.gson.annotations.SerializedName;
import com.grishberg.obmenogram.data.service.ApiConst;

/**
 * Created on 10.08.15.
 *
 * @author g
 */
public class UsersInPhoto {
    public static final String TAG = UsersInPhoto.class.getSimpleName();
    @SerializedName(ApiConst.Fields.POSITION)
    public Position position;
    @SerializedName(ApiConst.Fields.USER)
    public User user;
}
