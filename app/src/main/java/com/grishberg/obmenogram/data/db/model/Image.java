package com.grishberg.obmenogram.data.db.model;
import com.grishberg.obmenogram.data.db.DbHelper;
import android.content.ContentValues;
import android.database.Cursor;

public class Image{
	private long id;
	private String imageId;
	private long createdTime;
	private String thumbnail;
	private int thumbnailWidth;
	private int thumbnailHeight;
	private String standardResolution;
	private int standardWidth;
	private int standardHeight;
	private String caption;
	private long userId;

	public Image(long id
			,String imageId
			,long createdTime
			,String thumbnail
			,int thumbnailWidth
			,int thumbnailHeight
			,String standardResolution
			,int standardWidth
			,int standardHeight
			,String caption
			,long userId ){
		this.id = id;
		this.imageId = imageId;
		this.createdTime = createdTime;
		this.thumbnail = thumbnail;
		this.thumbnailWidth = thumbnailWidth;
		this.thumbnailHeight = thumbnailHeight;
		this.standardResolution = standardResolution;
		this.standardWidth = standardWidth;
		this.standardHeight = standardHeight;
		this.caption = caption;
		this.userId = userId;
	}
	public static Image fromCursor(Cursor c){
		int idColId = c.getColumnIndex(DbHelper.COLUMN_ID);
		int imageIdId = c.getColumnIndex(DbHelper.IMAGE_IMAGE_ID);
		int createdTimeId = c.getColumnIndex(DbHelper.IMAGE_CREATED_TIME);
		int thumbnailId = c.getColumnIndex(DbHelper.IMAGE_THUMBNAIL);
		int thumbnailWidthId = c.getColumnIndex(DbHelper.IMAGE_THUMBNAIL_WIDTH);
		int thumbnailHeightId = c.getColumnIndex(DbHelper.IMAGE_THUMBNAIL_HEIGHT);
		int standardResolutionId = c.getColumnIndex(DbHelper.IMAGE_STANDARD_RESOLUTION);
		int standardWidthId = c.getColumnIndex(DbHelper.IMAGE_STANDARD_WIDTH);
		int standardHeightId = c.getColumnIndex(DbHelper.IMAGE_STANDARD_HEIGHT);
		int captionId = c.getColumnIndex(DbHelper.IMAGE_CAPTION);
		int userIdId = c.getColumnIndex(DbHelper.IMAGE_USER_ID);
		return new Image( 
			c.getLong(idColId)
			, c.getString(imageIdId)
			, c.getLong(createdTimeId)
			, c.getString(thumbnailId)
			, c.getInt(thumbnailWidthId)
			, c.getInt(thumbnailHeightId)
			, c.getString(standardResolutionId)
			, c.getInt(standardWidthId)
			, c.getInt(standardHeightId)
			, c.getString(captionId)
			, c.getLong(userIdId) );
	}
	public ContentValues buildContentValues(){
		ContentValues cv = new ContentValues();
		if (id >= 0) {
			cv.put(DbHelper.COLUMN_ID, id);
		}
		cv.put(DbHelper.IMAGE_IMAGE_ID, imageId);
		cv.put(DbHelper.IMAGE_CREATED_TIME, createdTime);
		cv.put(DbHelper.IMAGE_THUMBNAIL, thumbnail);
		cv.put(DbHelper.IMAGE_THUMBNAIL_WIDTH, thumbnailWidth);
		cv.put(DbHelper.IMAGE_THUMBNAIL_HEIGHT, thumbnailHeight);
		cv.put(DbHelper.IMAGE_STANDARD_RESOLUTION, standardResolution);
		cv.put(DbHelper.IMAGE_STANDARD_WIDTH, standardWidth);
		cv.put(DbHelper.IMAGE_STANDARD_HEIGHT, standardHeight);
		cv.put(DbHelper.IMAGE_CAPTION, caption);
		cv.put(DbHelper.IMAGE_USER_ID, userId);
		return cv;

	}

	//------------- getters ------------
	public String getImageId(){
		return imageId;
	}

	public long getCreatedTime(){
		return createdTime;
	}

	public String getThumbnail(){
		return thumbnail;
	}

	public int getThumbnailWidth(){
		return thumbnailWidth;
	}

	public int getThumbnailHeight(){
		return thumbnailHeight;
	}

	public String getStandardResolution(){
		return standardResolution;
	}

	public int getStandardWidth(){
		return standardWidth;
	}

	public int getStandardHeight(){
		return standardHeight;
	}

	public String getCaption(){
		return caption;
	}

	public long getUserId(){
		return userId;
	}

}
