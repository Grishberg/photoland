package com.grishberg.obmenogram.data.model;

/**
 * Created by g on 15.09.15.
 */
public class ImageBounds {
    private int left;
    private int right;
    private int top;
    private int bottom;

    public ImageBounds(int left, int right, int top, int bottom) {
        this.left = left;
        this.right = right;
        this.top = top;
        this.bottom = bottom;
    }

    public int getLeft() {
        return left;
    }

    public int getRight() {
        return right;
    }

    public int getTop() {
        return top;
    }

    public int getBottom() {
        return bottom;
    }
}
