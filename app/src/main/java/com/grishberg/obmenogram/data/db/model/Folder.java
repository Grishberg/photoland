package com.grishberg.obmenogram.data.db.model;
import com.grishberg.obmenogram.data.db.DbHelper;
import android.content.ContentValues;
import android.database.Cursor;

public class Folder{
	private long id;
	private String hashtag;
	private String name;

	public Folder(long id
			,String hashtag
			,String name ){
		this.id = id;
		this.hashtag = hashtag;
		this.name = name;
	}
	public static Folder fromCursor(Cursor c){
		int idColId = c.getColumnIndex(DbHelper.COLUMN_ID);
		int hashtagId = c.getColumnIndex(DbHelper.FOLDER_HASHTAG);
		int nameId = c.getColumnIndex(DbHelper.FOLDER_NAME);
		return new Folder( 
			c.getLong(idColId)
			, c.getString(hashtagId)
			, c.getString(nameId) );
	}
	public ContentValues buildContentValues(){
		ContentValues cv = new ContentValues();
		if (id >= 0) {
			cv.put(DbHelper.COLUMN_ID, id);
		}
		cv.put(DbHelper.FOLDER_HASHTAG, hashtag);
		cv.put(DbHelper.FOLDER_NAME, name);
		return cv;

	}

	//------------- getters ------------
	public String getHashtag(){
		return hashtag;
	}

	public String getName(){
		return name;
	}

}
