package com.grishberg.obmenogram.data.model;

import com.google.gson.annotations.SerializedName;
import com.grishberg.obmenogram.data.service.ApiConst;

import java.util.List;

/**
 * Created by g on 09.08.15.
 */
public class UsersFeedResponse {
    @SerializedName(ApiConst.Fields.PAGINATION)
    public Pagination pagination;

    @SerializedName(ApiConst.Fields.META)
    public Meta meta;

    @SerializedName(ApiConst.Fields.DATA)
    public List<Data> data;
}
