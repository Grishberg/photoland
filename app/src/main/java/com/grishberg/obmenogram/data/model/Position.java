package com.grishberg.obmenogram.data.model;

import com.google.gson.annotations.SerializedName;
import com.grishberg.obmenogram.data.service.ApiConst;

/**
 * Created on 10.08.15.
 *
 * @author g
 */
public class Position {
    public static final String TAG = Position.class.getSimpleName();
    @SerializedName(ApiConst.Fields.X)
    public double x;
    @SerializedName(ApiConst.Fields.Y)
    public double y;
}
