package com.grishberg.obmenogram.data.model;

import com.google.gson.annotations.SerializedName;
import com.grishberg.obmenogram.data.service.ApiConst;

public class Images {
	@SerializedName(ApiConst.Fields.STANDARD_RESOLUTION)
	public StandardResolution standardResolution;
	@SerializedName(ApiConst.Fields.LOW_RESOLUTION)
	public LowResolution lowResolution;
	@SerializedName(ApiConst.Fields.THUMBNAIL)
	public Thumbnail thumbnail;
}
