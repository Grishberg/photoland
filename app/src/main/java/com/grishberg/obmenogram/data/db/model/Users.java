package com.grishberg.obmenogram.data.db.model;
import com.grishberg.obmenogram.data.db.DbHelper;
import android.content.ContentValues;
import android.database.Cursor;

public class Users{
	private long id;
	private String userId;
	private String username;
	private String profilepicture;
	private String fullname;

	public Users(long id
			,String userId
			,String username
			,String profilepicture
			,String fullname ){
		this.id = id;
		this.userId = userId;
		this.username = username;
		this.profilepicture = profilepicture;
		this.fullname = fullname;
	}
	public static Users fromCursor(Cursor c){
		int idColId = c.getColumnIndex(DbHelper.COLUMN_ID);
		int userIdId = c.getColumnIndex(DbHelper.USERS_USER_ID);
		int usernameId = c.getColumnIndex(DbHelper.USERS_USERNAME);
		int profilepictureId = c.getColumnIndex(DbHelper.USERS_PROFILE_PICTURE);
		int fullnameId = c.getColumnIndex(DbHelper.USERS_FULL_NAME);
		return new Users( 
			c.getLong(idColId)
			, c.getString(userIdId)
			, c.getString(usernameId)
			, c.getString(profilepictureId)
			, c.getString(fullnameId) );
	}
	public ContentValues buildContentValues(){
		ContentValues cv = new ContentValues();
		if (id >= 0) {
			cv.put(DbHelper.COLUMN_ID, id);
		}
		cv.put(DbHelper.USERS_USER_ID, userId);
		cv.put(DbHelper.USERS_USERNAME, username);
		cv.put(DbHelper.USERS_PROFILE_PICTURE, profilepicture);
		cv.put(DbHelper.USERS_FULL_NAME, fullname);
		return cv;

	}

	//------------- getters ------------
	public String getUserId(){
		return userId;
	}

	public String getUsername(){
		return username;
	}

	public String getProfilepicture(){
		return profilepicture;
	}

	public String getFullname(){
		return fullname;
	}

}
