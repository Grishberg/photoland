package com.grishberg.obmenogram.data.model;

import com.google.gson.annotations.SerializedName;
import com.grishberg.obmenogram.data.service.ApiConst;

public class Thumbnail {
	@SerializedName(ApiConst.Fields.URL)
	public String url;
	@SerializedName(ApiConst.Fields.WIDTH)
	public int width;
	@SerializedName(ApiConst.Fields.HEIGHT)
	public int height;
}
