package com.grishberg.obmenogram.data.service.async_workers;

import android.text.TextUtils;
import android.util.Log;

import com.grishberg.obmenogram.App;
import com.grishberg.obmenogram.data.db.model.Feed;
import com.grishberg.obmenogram.data.db.model.Image;
import com.grishberg.obmenogram.data.db.model.Users;
import com.grishberg.obmenogram.data.model.Data;
import com.grishberg.obmenogram.data.model.UsersFeedResponse;
import com.grishberg.obmenogram.data.service.Api;
import com.grishberg.obmenogram.data.service.exceptions.WrongDataException;
import com.grishberg.obmenogram.framework.IApiService;
import com.grishberg.obmenogram.framework.Utils;
import com.grishberg.obmenogram.framework.interfaces.IThreadObserver;
import com.grishberg.obmenogram.framework.multithreading.PriorityRunnable;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by g on 20.09.15.
 */
public class FeedRunnable implements PriorityRunnable {
    private static final String TAG = FeedRunnable.class.getSimpleName();
    private static final int MESSAGE_POST_RESULT = 1;
    private static final int ITEMS_COUNT = 10;
    private static final String TYPE_IMAGE = "image";
    private Api mRetrofitApi;
    private IApiService mApiService;
    private IThreadObserver mRunObserver;
    private String mAccessToken;
    private String mNextMaxId;
    private int mPriority;
    private String mTag;
    private int mTaskId;

    public FeedRunnable(int priority, Api api
            , IApiService apiService
            , IThreadObserver mRunObserver
            , String token, String nexMaxId) {
        mApiService = apiService;
        mRetrofitApi = api;
        mAccessToken = token;
        mNextMaxId = nexMaxId;
        mPriority = priority;
    }

    @Override
    public int getPriority() {
        return mPriority;
    }

    /**
     * do main work
     *
     * @return
     * @throws Exception
     */
    @Override
    public void run() {
        int newElementsCount = 0;
        UsersFeedResponse feedResponse = null;
        Call<UsersFeedResponse> call = null;
        try {
            //TODO: fix pagination
            if (TextUtils.isEmpty(mNextMaxId)) {
                call = mRetrofitApi.getUsersFeed(mAccessToken, ITEMS_COUNT);
            } else {
                call = mRetrofitApi.getUsersNextFeed(mAccessToken
                        , mNextMaxId, ITEMS_COUNT);
            }
            Response<UsersFeedResponse> resp = call.execute();
            feedResponse = resp.body();
            if (feedResponse == null) {
                throw new WrongDataException("Response body is empty");
            }
            newElementsCount = parseFeedResponse(newElementsCount, feedResponse);
        } catch (WrongDataException e) {
            Log.e(TAG, "run: ", e);
        } catch (Exception e) {
            Log.e(TAG, "run: ", e);
        }

        if (mApiService != null) {
            mApiService.onResponseFeed(newElementsCount);
        }

        if (mRunObserver != null) {
            mRunObserver.onTaskDone(mTag, mTaskId);
        }
    }

    /**
     * Извлечь данные из ответа
     *
     * @param newElementsCount
     * @param feedResponse
     * @return
     */
    private int parseFeedResponse(int newElementsCount, UsersFeedResponse feedResponse) {
        // put it to DB
        for (Data data : feedResponse.data) {
            if (data.type.equals(TYPE_IMAGE)) {
                long timeLong = Long.valueOf(data.createdTime);
                String caption = "";
                long userId = -1;
                long imageId = -1;
                if (data.caption != null) {
                    caption = data.caption.text;
                }
                Users userElement = null;
                Image imageElement = null;
                if (data.user != null) {
                    userElement = new Users(-1, data.user.id
                            , data.user.username
                            , data.user.profilePicture
                            , data.user.fullName);
                    userId = App.insertUsers(userElement);
                }
                if (data.images != null) {
                    Log.d(TAG, "image src: w=" + data.images.standardResolution.width +
                            " h=" + data.images.standardResolution.height);
                    imageElement = new Image(-1, data.id, timeLong * 1000
                            , data.images.thumbnail.url
                            , data.images.thumbnail.width
                            , data.images.thumbnail.height
                            , data.images.standardResolution.url
                            , 0
                            , 0
                            , caption
                            , userId);
                    imageId = App.insertImage(imageElement);
                }

                String[] nextUrlParams = Utils.extractNextToken(feedResponse.pagination.nextUrl);
                if (nextUrlParams == null) {
                    nextUrlParams = new String[]{"", ""};
                }
                Feed feed = new Feed(-1, data.id
                        , imageId
                        , nextUrlParams[0]
                        , nextUrlParams[1]
                        , timeLong * 1000

                        , data.user.username
                        , data.user.fullName
                        , data.user.profilePicture

                        , caption
                        , data.images.standardResolution.url
                        , 0
                        , 0);
                long feedId = App.insertFeed(feed);
                Log.d(TAG, "insert feed result=" + feedId + " id=" + data.id);
                if (feedId >= 0) {
                    mApiService.loadImage(feedId, data.images.standardResolution.url);
                    newElementsCount++;
                }
            }
        }
        return newElementsCount;
    }
}
