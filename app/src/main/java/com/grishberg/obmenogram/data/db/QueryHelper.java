package com.grishberg.obmenogram.data.db;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import com.grishberg.obmenogram.App;
import com.grishberg.obmenogram.data.db.model.Feed;
import com.grishberg.obmenogram.data.db.model.Folder;
import com.grishberg.obmenogram.data.db.model.Image;
import com.grishberg.obmenogram.data.db.model.Users;

/**
 * Created by g on 17.09.15.
 */
public class QueryHelper {
    private static final String TAG = QueryHelper.class.getSimpleName();
    public static void updateImageSize(final long id, final int w, final int h) {
        Log.d("QueryHelper", "updateImageSize id=" + id + " w=" + w + " h=" + h);
        ContentValues values = new ContentValues(2);
        values.put(DbHelper.FEED_IMAGE_WIDTH, w);
        values.put(DbHelper.FEED_IMAGE_HEIGHT, h);

        String table = DbHelper.TABLE_FEED;
        SQLiteDatabase db = DbHelper.getInstance(App.getAppContext()).getWritableDatabase();
        int res = App.getAppContext().getContentResolver()
                .update(AppContentProvider.CONTENT_URI_FEED, values
                        , DbHelper.COLUMN_ID + " = ?", new String[]{String.valueOf(id)});
        Log.d(TAG, "on update ImageSize id="+id+" res="+res+" w="+w+" h="+h);
        /*
        new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void[] params) {
                ContentValues values = new ContentValues(2);
                values.put(DbHelper.IMAGE_STANDARD_WIDTH, w);
                values.put(DbHelper.IMAGE_STANDARD_HEIGHT, h);
                App.getAppContext().getContentResolver()
                        .update(AppContentProvider.CONTENT_URI_IMAGE,values
                                ,DbHelper.COLUMN_ID+" = ?",new String[]{String.valueOf(id)});
                return null;
            }
        }.execute();
        */
    }

    public static long insertFolder(Folder value) {
        Uri result = App.getAppContext().getContentResolver()
                .insert(AppContentProvider.CONTENT_URI_FOLDER
                        , value.buildContentValues());
        return Long.valueOf(result.getLastPathSegment());
    }

    public static long insertUsers(Users value) {
        Uri result = App.getAppContext().getContentResolver()
                .insert(AppContentProvider.CONTENT_URI_USERS
                        , value.buildContentValues());
        return Long.valueOf(result.getLastPathSegment());
    }

    public static long insertImage(Image value) {
        Uri result = App.getAppContext().getContentResolver()
                .insert(AppContentProvider.CONTENT_URI_IMAGE
                        , value.buildContentValues());
        return Long.valueOf(result.getLastPathSegment());
    }

    public static long insertFeed(Feed value) {
        Uri result = App.getAppContext().getContentResolver()
                .insert(AppContentProvider.CONTENT_URI_FEED
                        , value.buildContentValues());
        return Long.valueOf(result.getLastPathSegment());
    }

    public static void clearDb() {
        App.getAppContext().getContentResolver()
                .delete(AppContentProvider.CONTENT_URI_FEED, null, null);
        App.getAppContext().getContentResolver()
                .delete(AppContentProvider.CONTENT_URI_FOLDER, null, null);
        App.getAppContext().getContentResolver()
                .delete(AppContentProvider.CONTENT_URI_USERS, null, null);
        App.getAppContext().getContentResolver()
                .delete(AppContentProvider.CONTENT_URI_IMAGE, null, null);
    }
}
