package com.grishberg.obmenogram.data.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "cache.db";
    private static final int DB_VERSION = 3;
    public static final String COLUMN_ID = "_id";
    // folder
    public static final String TABLE_FOLDER = "folder";
    public static final String FOLDER_HASHTAG = "hashtag";
    public static final String FOLDER_NAME = "name";
    // users
    public static final String TABLE_USERS = "users";
    public static final String USERS_USER_ID = "user_id";
    public static final String USERS_USERNAME = "username";
    public static final String USERS_PROFILE_PICTURE = "profilePicture";
    public static final String USERS_FULL_NAME = "fullName";
    // image
    public static final String TABLE_IMAGE = "image";
    public static final String IMAGE_IMAGE_ID = "image_id";
    public static final String IMAGE_CREATED_TIME = "created_time";
    public static final String IMAGE_THUMBNAIL = "thumbnail";
    public static final String IMAGE_THUMBNAIL_WIDTH = "thumbnail_width";
    public static final String IMAGE_THUMBNAIL_HEIGHT = "thumbnail_height";
    public static final String IMAGE_STANDARD_RESOLUTION = "standard_resolution";
    public static final String IMAGE_STANDARD_WIDTH = "standard_width";
    public static final String IMAGE_STANDARD_HEIGHT = "standard_height";
    public static final String IMAGE_CAPTION = "caption";
    public static final String IMAGE_USER_ID = "user_id";
    // feed
    public static final String TABLE_FEED = "feed";
    public static final String FEED_FEED_ID = "feed_id";
    public static final String FEED_IMAGE_ID = "image_id";
    public static final String FEED_NEXT_ACCESS_TOKEN = "nextUrl";
    public static final String FEED_NEXT_MAX_ID = "nextMaxId";
    public static final String FEED_CREATED_TIME = "dt";
    public static final String FEED_IMAGE_URL = "image_url";
    public static final String FEED_IMAGE_WIDTH = "image_width";
    public static final String FEED_IMAGE_HEIGHT = "image_height";
    public static final String FEED_CAPTION = "caption";
    public static final String FEED_USERNAME = "username";
    public static final String FEED_PROFILE_PICTURE = "profilePicture";
    public static final String FEED_FULL_NAME = "fullName";


    // folder
    private static final String CREATE_TABLE_FOLDER = "" +
            "CREATE TABLE " + TABLE_FOLDER + "(" +
            COLUMN_ID + " integer primary key," +
            FOLDER_HASHTAG + " TEXT ," +
            FOLDER_NAME + " TEXT UNIQUE " +
            ");";
    // users
    private static final String CREATE_TABLE_USERS = "" +
            "CREATE TABLE " + TABLE_USERS + "(" +
            COLUMN_ID + " integer primary key," +
            USERS_USER_ID + " TEXT UNIQUE ," +
            USERS_USERNAME + " TEXT ," +
            USERS_PROFILE_PICTURE + " TEXT ," +
            USERS_FULL_NAME + " TEXT " +
            ");";
    // image
    private static final String CREATE_TABLE_IMAGE = "" +
            "CREATE TABLE " + TABLE_IMAGE + "(" +
            COLUMN_ID + " integer primary key," +
            IMAGE_IMAGE_ID + " TEXT UNIQUE ," +
            IMAGE_CREATED_TIME + " INTEGER ," +
            IMAGE_THUMBNAIL + " TEXT ," +
            IMAGE_THUMBNAIL_WIDTH + " INTEGER ," +
            IMAGE_THUMBNAIL_HEIGHT + " INTEGER ," +
            IMAGE_STANDARD_RESOLUTION + " TEXT ," +
            IMAGE_STANDARD_WIDTH + " INTEGER ," +
            IMAGE_STANDARD_HEIGHT + " INTEGER ," +
            IMAGE_CAPTION + " TEXT ," +
            IMAGE_USER_ID + " INTEGER " +
            ");";
    // feed
    private static final String CREATE_TABLE_FEED = "" +
            "CREATE TABLE " + TABLE_FEED + "(" +
            COLUMN_ID + " integer primary key," +
            FEED_FEED_ID + " TEXT UNIQUE ," +
            FEED_IMAGE_ID + " INTEGER ," +
            FEED_NEXT_ACCESS_TOKEN + " TEXT ," +
            FEED_NEXT_MAX_ID + " TEXT ," +
            FEED_CREATED_TIME + " INTEGER, " +
            FEED_USERNAME + " TEXT ," +
            FEED_FULL_NAME + " TEXT ," +
            FEED_PROFILE_PICTURE + " TEXT ," +
            FEED_CAPTION + " TEXT ," +
            FEED_IMAGE_URL + " TEXT ," +
            FEED_IMAGE_WIDTH + " INTEGER ," +
            FEED_IMAGE_HEIGHT + " INTEGER " +
            ");";
    private static DbHelper dbHelper;

    public static DbHelper getInstance(Context context){
        if(dbHelper == null){
            dbHelper = new DbHelper(context);
        }
        return dbHelper;
    }
    public DbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String[] CREATES = {
                CREATE_TABLE_FOLDER,
                CREATE_TABLE_USERS,
                CREATE_TABLE_IMAGE,
                CREATE_TABLE_FEED
        };
        for (final String table : CREATES) {
            db.execSQL(table);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String[] TABLES = {
                TABLE_FOLDER,
                TABLE_USERS,
                TABLE_IMAGE,
                TABLE_FEED
        };
        for (final String table : TABLES) {
            db.execSQL("DROP TABLE IF EXISTS " + table);
        }
        onCreate(db);
    }
}
