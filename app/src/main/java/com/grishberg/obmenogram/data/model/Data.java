package com.grishberg.obmenogram.data.model;

import com.google.gson.annotations.SerializedName;
import com.grishberg.obmenogram.data.service.ApiConst;

import java.util.List;

public class Data {
	@SerializedName(ApiConst.Fields.IMAGES)
	public Images images;
	@SerializedName(ApiConst.Fields.CAPTION)
	public Caption caption;
	@SerializedName(ApiConst.Fields.LOCATION)
	public Location location;
	@SerializedName(ApiConst.Fields.LIKES)
	public Likes likes;
	@SerializedName(ApiConst.Fields.ATTRIBUTION)
	public String attribution;
	@SerializedName(ApiConst.Fields.COMMENTS)
	public Comments comments;
	@SerializedName(ApiConst.Fields.USER_HAS_LIKED)
	public boolean userHasLiked;
	@SerializedName(ApiConst.Fields.LINK)
	public String link;
	@SerializedName(ApiConst.Fields.TYPE)
	public String type;
	@SerializedName(ApiConst.Fields.USERS_IN_PHOTO)
	public List<UsersInPhoto> usersInPhoto;
	@SerializedName(ApiConst.Fields.FILTER)
	public String filter;
	@SerializedName(ApiConst.Fields.ID)
	public String id;
	@SerializedName(ApiConst.Fields.TAGS)
	public List<String> tags;
	@SerializedName(ApiConst.Fields.USER)
	public User user;
	@SerializedName(ApiConst.Fields.CREATED_TIME)
	public String createdTime;
}
