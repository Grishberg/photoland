package com.grishberg.obmenogram.data.model;

import com.google.gson.annotations.SerializedName;
import com.grishberg.obmenogram.data.service.ApiConst;

public class Caption {
	@SerializedName(ApiConst.Fields.CREATED_TIME)
	public String createdTime;
	@SerializedName(ApiConst.Fields.ID)
	public String id;
	@SerializedName(ApiConst.Fields.TEXT)
	public String text;
	@SerializedName(ApiConst.Fields.FROM)
	public From from;
}
