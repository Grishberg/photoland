package com.grishberg.obmenogram.data.service.async_workers;

import android.os.*;

import com.grishberg.obmenogram.framework.IApiService;
import com.grishberg.obmenogram.framework.RestRequest;
import com.grishberg.obmenogram.framework.interfaces.IThreadObserver;

/**
 * Created by g on 20.09.15.
 */
public class RequestRunnable implements Runnable {
    private static final int MESSAGE_POST_RESULT = 1;
    private RestRequest mRequest;
    private IThreadObserver mRunObserver;
    private IApiService mApiService;
    private int mPriority;
    private MessageHandler mHandler;

    public RequestRunnable(IThreadObserver runObserver
            , IApiService apiService
            , RestRequest request, int priority) {
        mRequest = request;
        mApiService = apiService;
        mRunObserver = runObserver;
        mPriority = priority;
        if (priority == -1) {
            mPriority = android.os.Process.THREAD_PRIORITY_DEFAULT;
        }
        mHandler = new MessageHandler();
        mHandler.setOnDoneObserver(request.getTag(), request.getId(), mRunObserver);
    }

    /**
     * do main work
     *
     * @return
     * @throws Exception
     */
    @Override
    public void run() {
        if (mRequest == null) return;
        android.os.Process.setThreadPriority(mPriority);
        mRequest.onRequest();
        mRunObserver.onTaskDone(mRequest.getTag(), mRequest.getId());
        postResult(null);
    }

    /**
     * send result in main thread
     *
     * @param result
     */
    private void postResult(Object result) {
        Message message = mHandler.obtainMessage(MESSAGE_POST_RESULT, result);
        message.sendToTarget();
    }

    private static class MessageHandler extends Handler {
        private IThreadObserver observer;
        private String tag;
        private int id;

        public void setOnDoneObserver(String tag, int id, IThreadObserver observer) {
            this.observer = observer;
            this.id = id;
            this.tag = tag;
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (observer != null) {
                observer.onTaskDone(tag, id/*msg.obj*/);
            }
        }
    }
}
