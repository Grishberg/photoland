package com.grishberg.obmenogram.data.service.async_workers;

import android.graphics.Bitmap;

import com.grishberg.obmenogram.data.db.QueryHelper;
import com.grishberg.obmenogram.framework.IImageLoaderObserver;
import com.grishberg.obmenogram.framework.multithreading.PriorityRunnable;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by g on 21.09.15.
 */
public class ImageLoaderRunnable implements PriorityRunnable {
    private static final int MESSAGE_POST_RESULT = 1;
    private String mUri;
    private long mId;
    private IImageLoaderObserver mRunObserver;
    private int mPriority;

    public ImageLoaderRunnable(int priority,IImageLoaderObserver runObserver, String uri, long id) {
        mUri = uri;
        mId = id;
        mRunObserver = runObserver;
        mPriority = priority;
        if (priority == -1) {
            mPriority = android.os.Process.THREAD_PRIORITY_DEFAULT;
        }
    }

    @Override
    public int getPriority() {
        return mPriority;
    }

    /**
     * do main work
     *
     * @return
     * @throws Exception
     */
    @Override
    public void run() {
        if (mUri == null) return;
        Bitmap bitmap = ImageLoader.getInstance()
                .loadImageSync(mUri);
        QueryHelper.updateImageSize(mId, bitmap.getWidth(), bitmap.getHeight());

        mRunObserver.onImageLoaded(mId, mUri);
    }
}

