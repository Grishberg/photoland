package com.grishberg.obmenogram.data.model;

import com.google.gson.annotations.SerializedName;
import com.grishberg.obmenogram.data.service.ApiConst;

public class Location {
	@SerializedName(ApiConst.Fields.LATITUDE)
	public double latitude;
	@SerializedName(ApiConst.Fields.LONGITUDE)
	public double longitude;
}
