package com.grishberg.obmenogram.data.service.async_workers;

import android.os.AsyncTask;

import com.grishberg.obmenogram.App;
import com.grishberg.obmenogram.data.model.auth.AccessToken;
import com.grishberg.obmenogram.data.service.Api;
import com.grishberg.obmenogram.data.service.ApiConst;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by g on 15.09.15.
 */
public class AuthWorker extends AsyncTask<Void, Void, AccessToken> {
    private String mCode;
    private Api mApiService;
    private IOnAuthListener mListener;

    public AuthWorker(Api apiService, IOnAuthListener listener, String code) {
        mCode = code;
        mApiService = apiService;
        mListener = listener;
    }

    @Override
    protected AccessToken doInBackground(Void... params) {
        AccessToken token = null;

        try {
            Call<AccessToken> call = mApiService.requestAccessToken(ApiConst.Api.CLIENT_ID
                    , ApiConst.Api.CLIENT_SECRET
                    , ApiConst.Auth.AUTHORIZATION_CODE
                    , ApiConst.Api.REDIRECT_URI
                    , mCode);
            Response<AccessToken> response = call.execute();
            if (response.body() == null) {
                //TODO: throw AuthException
            }
            token = response.body();
        } catch (Exception e) {
            e.printStackTrace();
        }

        App.clearDb();
        return token;
    }

    @Override
    protected void onPostExecute(AccessToken accessToken) {
        super.onPostExecute(accessToken);
        if (mListener != null && accessToken != null) {
            mListener.onAuth(accessToken.accessToken
                    , accessToken.user.username, accessToken.user.profilePicture);
        }
    }

    public void setListener(IOnAuthListener listener) {
        mListener = listener;
    }

    public interface IOnAuthListener {
        void onAuth(String accessToken, String userName, String userPicture);

        void onAuthFail();
    }
}
