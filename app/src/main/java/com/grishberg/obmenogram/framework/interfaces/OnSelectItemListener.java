package com.grishberg.obmenogram.framework.interfaces;

/**
 * Created by g on 01.10.15.
 */
public interface OnSelectItemListener{
    void selectItem(int position);
}