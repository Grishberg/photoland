package com.grishberg.obmenogram.framework.interfaces;

import android.support.v4.app.Fragment;

/**
 * Created by g on 22.09.15.
 */
public interface IMainActivityInteraction {
    void onRegister(Fragment fragment);
    void onUnregister(Fragment fragment);
    void onAuth(String code);
    void onGetNextFeed(String nextId);
}
