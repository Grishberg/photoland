package com.grishberg.obmenogram.framework;

/**
 * Created by g on 19.09.15.
 */
public abstract class RestRequest<T> {
    private static int id;
    private String mTag;

    public RestRequest() {
        this("");
    }

    public RestRequest(String tag) {
        mTag = tag;
        id++;
        if(id > 0xFFFF){
            id = 0;
        }
    }

    /**
     * implement rest query from retrofit service
     * @return
     */
    public abstract T onRequest();

    /**
     * implement async logic after response
     * @param result
     */
    public void onSuccess(T result) {
    }

    /**
     * implement sync logic in main thread
     * @param result
     */
    public void onDone(T result) {
    }

    public void onFail(String message, int code) {

    }

    public String getTag() {
        return mTag;
    }

    public int getId(){
        return id;
    }
}
