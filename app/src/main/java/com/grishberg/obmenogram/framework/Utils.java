package com.grishberg.obmenogram.framework;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Point;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

import com.grishberg.obmenogram.R;
import com.grishberg.obmenogram.data.service.ApiConst;

/**
 * Created by g on 23.08.15.
 */
public class Utils {
    private static int sTheme;
    public final static int THEME_DEFAULT = 0;
    public final static int THEME_DARK = 1;
    public final static int THEME_WHITE = 2;
    public final static int COLOR_WHITE = -1;

    /**
     * Set the theme of the Activity, and restart it by creating a new Activity of the same type.
     */
    public static void changeToTheme(Activity activity, int theme) {
        sTheme = theme;
        activity.finish();
        activity.startActivity(new Intent(activity, activity.getClass()));
    }

    /**
     * Set the theme of the activity, according to the configuration.
     */
    public static void onActivityCreateSetTheme(Activity activity) {
        switch (sTheme) {
            default:
            case THEME_DEFAULT:
                activity.setTheme(R.style.PurpleTheme);
                break;
            case THEME_DARK:
                activity.setTheme(R.style.BlackTheme);
                break;
            case THEME_WHITE:
                activity.setTheme(R.style.WhiteTheme);
                break;
        }
    }

    //--------------------------------------------------------------------------------------
    public static Point getScreenSize(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;
    }

    public static String[] extractNextToken(String url) {
        if(TextUtils.isEmpty(url)){
            return null;
        }
        Uri uri = Uri.parse(url);
        String at = uri.getQueryParameter(ApiConst.Fields.ACCESS_TOKEN);
        String nextId = uri.getQueryParameter(ApiConst.Fields.MAX_ID);
        if (TextUtils.isEmpty(at) || TextUtils.isEmpty(nextId)) {
            return null;
        }
        return new String[]{at, nextId};
    }

    public static void showMessage(Activity activity, int stringId) {
        View view = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        if (view != null) {
            Snackbar.make(view, stringId, Snackbar.LENGTH_LONG).show();
        }
    }

    public static Bitmap getImageBounds(Bitmap src) {
        int w = src.getWidth();
        int h = src.getHeight();

        int[] srcPixels = new int[w * h];
        src.getPixels(srcPixels, 0, w, 0, 0, w, h);

        int x = 0;
        int y = h / 2;
        int leftBound = -1;
        int rightBound = -1;
        int topBound = -1;
        int bottomBound = -1;
        int cornerPixelColor = srcPixels.length > 0 ? srcPixels[0] : -1;

        for (x = 0; x < w; x++) {
            int colorLeft = srcPixels[x + y * w];
            int colorRight = srcPixels[w - x - 1 + y * w];
            if (colorLeft != cornerPixelColor && leftBound == -1) {
                leftBound = x;
            }
            if (colorRight != cornerPixelColor && rightBound == -1) {
                rightBound = w - x - 1;
            }
            if (leftBound != -1 && rightBound != -1) {
                break;
            }
        }

        x = w / 2;
        for (y = 0; y < h; y++) {
            int colorTop = srcPixels[x + y * w];
            int colorBottom = srcPixels[w - x - 1 + y * w];
            if (colorTop != cornerPixelColor && topBound == -1) {
                topBound = y;
            }
            if (colorBottom != cornerPixelColor && bottomBound == -1) {
                bottomBound = h - y - 1;
            }
            if (bottomBound != -1 && topBound != -1) {
                break;
            }
        }
        int newWidth = rightBound - leftBound + 1;
        int newHeight = bottomBound - topBound + 1;
        Matrix matrix = new Matrix();
        return Bitmap.createBitmap(src, leftBound, topBound, newWidth, newHeight, matrix, true);
        //new ImageBounds(leftBound, rightBound, topBound, bottomBound);
    }
}
