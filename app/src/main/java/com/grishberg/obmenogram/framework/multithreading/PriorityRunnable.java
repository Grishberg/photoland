package com.grishberg.obmenogram.framework.multithreading;

/**
 * Created by g on 06.10.15.
 */
public interface PriorityRunnable extends Runnable{
    int getPriority();
}