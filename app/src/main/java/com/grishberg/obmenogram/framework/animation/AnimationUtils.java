package com.grishberg.obmenogram.framework.animation;

import android.animation.ObjectAnimator;
import android.support.v7.widget.RecyclerView;

/**
 * Created by g on 03.10.15.
 */
public class AnimationUtils {
    public static final int DURATION = 1000;
    public static final int ANIMATION_HEIGHT = 300;
    public static void animateItem(RecyclerView.ViewHolder holder, boolean isDownDirection){
        ObjectAnimator animator = ObjectAnimator.ofFloat(holder.itemView
                , "translationY"
                ,isDownDirection ? ANIMATION_HEIGHT : - ANIMATION_HEIGHT, 0);
        animator.setDuration(DURATION);
        animator.start();
    }
}
