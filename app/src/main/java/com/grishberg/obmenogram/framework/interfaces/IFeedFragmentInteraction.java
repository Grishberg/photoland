package com.grishberg.obmenogram.framework.interfaces;

/**
 * Created by g on 22.09.15.
 */
public interface IFeedFragmentInteraction {
    void onGetFeedResponse();
    void onImageLoaded(long id);
}
