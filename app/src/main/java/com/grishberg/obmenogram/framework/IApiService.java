package com.grishberg.obmenogram.framework;

/**
 * Created by g on 21.09.15.
 */
public interface IApiService {
    void onGetFeed(String token, String nextId);
    void doAuthRequest(String code);
    void loadImage(long id, String url);
    void onResponseFeed(int count);
}
