package com.grishberg.obmenogram.framework;

/**
 * Created by g on 21.09.15.
 */
public interface IImageLoaderObserver {
    void onImageLoaded(long id, String uri);
}
