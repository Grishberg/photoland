package com.grishberg.obmenogram.framework;

/**
 * Created by g on 21.09.15.
 */
public class Const {
    public static final String SERVICE_ACTION_FEED_RESPONSE = "restServiceAction";
    public static final String SERVICE_ACTION_AUTH = "authServiceAction";
    public static final String SERVICE_ACTION_IMAGE_LOADED = "imageLoadedServiceAction";
    public static final String REST_SERVICE_INTENT_FILTER = "localIntentFilter";
    public static final String REST_SERVICE_RESPONSE_CODE_EXTRA = "extraResponseCode";
    public static final String REST_SERVICE_RESPONSE_ID_EXTRA = "extraResponseId";
    public static final String EXTRA_ACCESS_TOKEN = "extraAccessToken";
    public static final String EXTRA_USER_NAME = "extraUserName";
    public static final String EXTRA_USER_PICTURE = "extraUserPic";
    public static final String EXTRA_IMAGE_ID = "extraImageId";
}
