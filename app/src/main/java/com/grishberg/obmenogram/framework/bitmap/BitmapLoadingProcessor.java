package com.grishberg.obmenogram.framework.bitmap;

import android.graphics.Bitmap;

import com.grishberg.obmenogram.framework.Utils;
import com.nostra13.universalimageloader.core.process.BitmapProcessor;

/**
 * Created by g on 16.09.15.
 */
public class BitmapLoadingProcessor implements BitmapProcessor {
    @Override
    public Bitmap process(Bitmap bitmap) {
        Bitmap newBitmap = Utils.getImageBounds(bitmap);
        return newBitmap;
    }
}
