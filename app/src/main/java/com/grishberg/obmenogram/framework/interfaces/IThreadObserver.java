package com.grishberg.obmenogram.framework.interfaces;

/**
 * Created by g on 27.09.15.
 */
public interface IThreadObserver {
    void onTaskDone(String tag, int id);
}
