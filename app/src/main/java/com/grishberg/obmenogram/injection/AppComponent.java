package com.grishberg.obmenogram.injection;

import com.grishberg.obmenogram.data.service.ApiService;
import com.grishberg.obmenogram.ui.activities.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by grishberg on 29.06.16.
 */
@Singleton
@Component(modules = {RestModule.class, AppModule.class})
public interface AppComponent {
    void inject(ApiService apiService);
    //void inject(MainActivity mainActivity);
}