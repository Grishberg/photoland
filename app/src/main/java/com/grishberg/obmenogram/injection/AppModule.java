package com.grishberg.obmenogram.injection;

import android.content.Context;

import com.grishberg.obmenogram.App;
import com.grishberg.obmenogram.data.service.ServiceWrapper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by grishberg on 29.06.16.
 */
@Module
public class AppModule {
    private final App application;

    public AppModule(App app) {
        this.application = app;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return application.getApplicationContext();
    }

    @Provides
    @Singleton
    ServiceWrapper provideApiService() {
        //application.getApplicationContext()
        return new ServiceWrapper();
    }
}