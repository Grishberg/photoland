package com.grishberg.obmenogram.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.grishberg.obmenogram.R;
import com.grishberg.obmenogram.data.db.model.Feed;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by g on 17.09.15.
 */
public class FeedInfoBlock extends LinearLayout {
    @Bind(R.id.btnLike)
    TriggerButton btnLike;
    @Bind(R.id.btnComment)
    ImageButton btnComment;
    @Bind(R.id.btnReply)
    ImageButton btnReply;
    @Bind(R.id.btnMenu)
    ImageButton btnMenu;
    @Bind(R.id.llComments)
    LinearLayout llComments;
    @Bind(R.id.tvLikesCount)
    TextView tvLikesCount;

    public FeedInfoBlock(Context context) {
        this(context, null);
    }

    public FeedInfoBlock(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(getContext(), R.layout.view_feed_block_info, this);
        ButterKnife.bind(this);
    }

    public void init(Feed feed) {
        tvLikesCount.setText(String.format(getContext().getResources()
                .getString(R.string.feed_info_likes_count), 0));
    }
}
