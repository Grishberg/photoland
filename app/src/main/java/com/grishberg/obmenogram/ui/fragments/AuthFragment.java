package com.grishberg.obmenogram.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.grishberg.obmenogram.R;
import com.grishberg.obmenogram.data.service.ApiConst;
import com.grishberg.obmenogram.framework.Utils;
import com.grishberg.obmenogram.framework.interfaces.IMainActivityInteraction;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by g on 13.09.15.
 */

public class AuthFragment extends BaseFragment {
    private static final String TAG = AuthFragment.class.getSimpleName();
    private IMainActivityInteraction mListener;
    @Bind(R.id.webView)
    WebView webView;
    private String mAccessToken;

    public static AuthFragment newInstance() {
        AuthFragment fragment = new AuthFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public AuthFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    private void initWebClient() {
        // need to get access token with OAuth2.0
        // set up webview for OAuth2 logindun
        //webView.clearCache(true);
        //webView.clearHistory();
        webView.setVisibility(View.VISIBLE);
        CookieSyncManager.createInstance(getActivity());
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();
        webView.setWebViewClient(new WebViewClient() {
            @Override

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith(ApiConst.Api.REDIRECT_URI)) {
                    // extract OAuth2 access_token appended in url
                    if (url.contains("code=")) {
                        String code = extractCode(url);
                        webView.setVisibility(View.GONE);
                        mListener.onAuth(code);
                    } else if(url.contains("error=access_denied")){
                        // extract error
                        Utils.showMessage(getActivity(), R.string.auth_access_denied);
                    }
                    // don't go to redirectUri
                    return true;
                }
                // load the webpage from url: login and grant access
                return super.shouldOverrideUrlLoading(view, url); // return false;
            }
        });

        // do OAuth2 login
        StringBuilder authBuilder = new StringBuilder(ApiConst.Api.END_POINT);
        authBuilder.append(ApiConst.Api.AUTH_REQUEST_CODE);
        authBuilder.append("?client_id=");
        authBuilder.append(ApiConst.Api.CLIENT_ID);
        authBuilder.append("&redirect_uri=");
        authBuilder.append(ApiConst.Api.REDIRECT_URI);
        authBuilder.append("&response_type=");
        authBuilder.append(ApiConst.Auth.CODE);
        webView.loadUrl(authBuilder.toString());
    }

    //TODO make ap
    private String extractToken(String url) {
        //Uri uri = Uri.parse(url);
        String[] lst = url.split("\\?");
        if(lst != null && lst.length > 1){
            String[] params = lst[1].split("=");
            if(params != null && params.length > 1){
                return params[1];
            }
        }
        return  null;
        //return uri.getQueryParameter(PARAM);
    }

    private String extractCode(String url) {
        //Uri uri = Uri.parse(url);
        String[] lst = url.split("\\?");
        if(lst != null && lst.length > 1){
            String[] params = lst[1].split("=");
            if(params != null && params.length > 1){
                return params[1];
            }
        }
        return  null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_auth, container, false);
        ButterKnife.bind(this, view);
        initWebClient();
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof IMainActivityInteraction){
            mListener = (IMainActivityInteraction) activity;
        } else {
            // throw
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
