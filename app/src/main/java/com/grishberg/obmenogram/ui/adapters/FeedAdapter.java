package com.grishberg.obmenogram.ui.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.grishberg.obmenogram.R;
import com.grishberg.obmenogram.data.db.model.Feed;
import com.grishberg.obmenogram.framework.animation.AnimationUtils;
import com.grishberg.obmenogram.ui.activities.ImageDetailActivity;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by g on 10.08.15.
 */
public class FeedAdapter extends CursorRecyclerViewAdapter<FeedAdapter.ViewHolder> {
    private static final String TAG = FeedAdapter.class.getSimpleName();
    private Context mContext;
    private IOnScrollItemListener mListener;
    private int mImageWidth = 0;
    private DisplayImageOptions mRoundedOptions;
    private FeedAdapter.ViewHolder.IMyViewHolderClicks mClickListener;
    private int mPrevousPosition = 0;

    public FeedAdapter(Context context, Cursor cursor, int width, IOnScrollItemListener listener) {
        super(context, cursor);
        mContext = context;
        mImageWidth = width;
        mListener = listener;

        mRoundedOptions = new DisplayImageOptions.Builder()
                .displayer(new RoundedBitmapDisplayer(50))
                        //.showImageOnLoading(R.drawable.empty_bg)
                        //.showImageForEmptyUri(R.drawable.camera)
                        //.showImageOnFail(R.drawable.ic_error)
                .cacheOnDisk(true)
                .build();

        mClickListener = new ViewHolder.IMyViewHolderClicks() {
            @Override
            public void onImageClick(long imageId, String imageUrl) {
                ImageDetailActivity.startActivity(mContext, imageUrl);
            }

            @Override
            public void onProfileClick(ImageView callerImage) {

            }
        };
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(R.id.tvFeedElementTitle)
        TextView tvTitle;
        @Bind(R.id.ivFeedElementPic)
        ImageView ivPic;
        @Bind(R.id.tvFeedUserName)
        TextView tvUserName;
        @Bind(R.id.ivFeedUserIcon)
        ImageView ivUserIcon;
        public long imageId;
        public String imageUrl;
        public IMyViewHolderClicks mListener;

        public ViewHolder(View view, int width, IMyViewHolderClicks listener) {
            super(view);
            ButterKnife.bind(this, view);
            mListener = listener;

            ivPic.setLayoutParams(
                    new LinearLayout.LayoutParams(width, width));

            ivPic.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v instanceof ImageView) {
                if (mListener != null) {
                    mListener.onImageClick(imageId, imageUrl);
                }
            }
        }

        public interface IMyViewHolderClicks {
            void onImageClick(long imageId, String imageUrl);

            void onProfileClick(ImageView callerImage);
        }
    }

    // создание вьюхолдера для элемента
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.feed_element, parent, false);
        ViewHolder vh = new ViewHolder(itemView, mImageWidth, mClickListener);
        return vh;
    }

    // заполнение каждого элемента данными
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Cursor cursor) {
        Feed item = Feed.fromCursor(cursor);
        viewHolder.tvTitle.setText(item.getCaption());
        viewHolder.tvUserName.setText(item.getUserName());
        if (mListener != null) {
            mListener.onScroll(item.getNextAccessToken(), item.getNextmaxid());
        }

        viewHolder.imageId = item.getImageId();

        // изменить размер ImageView, если были просчитаны размеры
        if (item.getImageWidth() > 0 && item.getImageHeight() > 0) {
            float scaleFactor = (float) mImageWidth / (float) item.getImageWidth();
            int newImageHeight = Math.round((float) item.getImageHeight() * scaleFactor);
            viewHolder.ivPic.setLayoutParams(
                    new LinearLayout.LayoutParams(mImageWidth, newImageHeight));
        } else {

            Log.d(TAG, "not loaded pic id=" + item.getImageId() + " url=" + item.getImageUrl());
        }

//        viewHolder.ivPic.setLayoutParams(
//                new LinearLayout.LayoutParams(mImageWidth, newImageHeight));

        if(viewHolder.imageUrl != null && viewHolder.imageUrl.equals(item.getImageUrl())){
            return;
        }
        viewHolder.imageUrl = item.getImageUrl();

        loadImage(item.getImageId(), viewHolder.ivPic, item.getImageUrl(), true);
        loadImage(item.getImageId(), viewHolder.ivUserIcon, item.getUserPic(), false);

        if(cursor.getPosition() > mPrevousPosition){
            AnimationUtils.animateItem(viewHolder, true);
        } else {
            AnimationUtils.animateItem(viewHolder, false);
        }
        mPrevousPosition = cursor.getPosition();
    }

    /**
     * asynchronously load image in mImageContainer
     *
     * @param url
     */
    public void loadImage(final long feedId, final ImageView container, String url
            , boolean isNeedFindBorders) {
        final ImageLoader imageLoader = ImageLoader.getInstance();

        //download and display image from url
        if (isNeedFindBorders) {
            imageLoader.cancelDisplayTask(container);
            imageLoader.displayImage(url, container);
            /*
            imageLoader.loadImage(url, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    //container.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    //container.setVisibility(View.VISIBLE);
                    
                    float scaleFactor = (float) mImageWidth / (float) loadedImage.getWidth();
                    int newImageHeight = Math.round((float) loadedImage.getHeight() * scaleFactor);
                    container.setLayoutParams(
                            new LinearLayout.LayoutParams(mImageWidth, newImageHeight));
                    QueryHelper.updateImageSize(feedId,loadedImage.getWidth(), loadedImage.getHeight());
                    imageLoader.displayImage(imageUri, container);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {

                }
            });
            */
        } else {
            imageLoader.cancelDisplayTask(container);
            imageLoader.displayImage(url, container, mRoundedOptions);
        }
        /*
                Picasso.with(mContext)
                .load(item.getStandardResolution())
                .placeholder(R.drawable.abc_item_background_holo_light)
                .into(viewHolder.ivPic);
         */
    }
    public void release(){
        final ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.stop();
        mClickListener = null;
    }
    public interface IOnScrollItemListener {
        void onScroll(String nextAccessToken, String nextMaxId);
    }
}
