package com.grishberg.obmenogram.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.grishberg.obmenogram.R;
import com.grishberg.obmenogram.ui.fragments.ImageDetailFragment;

public class ImageDetailActivity extends BaseActivity {
    private static final String ARG_IMAGE_URL = "argImageUrl";
    public static void startActivity(Context context, String imageUrl){
        Intent intent = new Intent(context, ImageDetailActivity.class);
        intent.putExtra(ARG_IMAGE_URL, imageUrl);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_detail);
        Intent intent = getIntent();
        String imageUrl = null;
        if(intent != null) {
            imageUrl = intent.getStringExtra(ARG_IMAGE_URL);
        }
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.flContainer, ImageDetailFragment.newInstance(imageUrl)
                            , ImageDetailFragment.class.getSimpleName())
                    .commit();
        }
    }

}
