package com.grishberg.obmenogram.ui.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.grishberg.obmenogram.App;
import com.grishberg.obmenogram.R;
import com.grishberg.obmenogram.framework.interfaces.IFeedFragmentInteraction;
import com.grishberg.obmenogram.framework.interfaces.IMainActivityInteraction;
import com.grishberg.obmenogram.framework.interfaces.OnSelectItemListener;
import com.grishberg.obmenogram.ui.fragments.AuthFragment;
import com.grishberg.obmenogram.ui.fragments.FeedFragment;

public class MainActivity extends BaseActivity implements IMainActivityInteraction
        , OnSelectItemListener {
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int MENU_MAIN = 0;
    private static final int MENU_SETTINGS = 1;
    private static final int MENU_LOGOFF = 2;
    private boolean mIsFirstRun;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private String[] mMenuItems;
    private String mAccessToken;
    private IFeedFragmentInteraction mFeedFragment;
    private DrawerItemClickListener mDrawerClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // enable ActionBar app icon to behave as action to toggle nav drawer

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mIsFirstRun = true;

        mMenuItems = getResources().getStringArray(R.array.menu_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // Set the adapter for the list view
        mDrawerList.setAdapter(new ArrayAdapter<>(this,
                R.layout.drawer_list_item, mMenuItems));
        // Set the list's click listener
        mDrawerClickListener = new DrawerItemClickListener();
        mDrawerClickListener.setListener(this);
        mDrawerList.setOnItemClickListener(mDrawerClickListener);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mAccessToken = App.getAccessToken();
        if (savedInstanceState == null) {
            mIsFirstRun = true;
        }
    }

    @Override
    protected void onBound() {
        super.onBound();
        if (mIsFirstRun) {
            mAccessToken = App.getAccessToken();
            if (mAccessToken == null) {
                getSupportActionBar().setTitle(App.getUserName());
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.flContainer, AuthFragment.newInstance()
                                , AuthFragment.class.getSimpleName())
                        .commit();
            } else {
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.flContainer, FeedFragment.newInstance()
                                , FeedFragment.class.getSimpleName())
                        .commit();
            }
            mIsFirstRun = false;
        }
    }

    @Override
    public void selectItem(int position) {
        // update the main content by replacing fragments
        switch (position) {
            case MENU_MAIN:
                getSupportActionBar().setTitle(App.getUserName());
                //TODO: set icon
                //App.setUserName(event.getAccessToken().user.username);
                //App.setUserPicture(event.getAccessToken().user.profilePicture);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.flContainer, FeedFragment.newInstance()
                                , FeedFragment.class.getSimpleName())
                        .commit();
                break;

            case MENU_SETTINGS:
                break;

            case MENU_LOGOFF:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.flContainer, AuthFragment.newInstance()
                                , AuthFragment.class.getSimpleName())
                        .commit();
                break;
        }
        // update selected item and title, then close the drawer
        mDrawerList.setItemChecked(position, true);
        setTitle(mMenuItems[position]);
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    public void onResponseAccessToken(String accessToken, String username, String profilePicture) {
        Log.d(TAG, "onResponseAT =" + accessToken);
        mAccessToken = accessToken;
        App.setAccessToken(accessToken);
        App.setUserName(username);
        App.setUserPicture(profilePicture);
        selectItem(MENU_MAIN);
    }

    @Override
    protected void onGetFeedResponse() {
        super.onGetFeedResponse();
        if (mFeedFragment != null) {
            mFeedFragment.onGetFeedResponse();
        }
    }

    @Override
    protected void onImageLoaded(long id) {
        super.onImageLoaded(id);
        if (mFeedFragment != null) {
            mFeedFragment.onImageLoaded(id);
        }
    }

    @Override
    public void onAuth(String code) {
        doAuth(code);
    }

    @Override
    public void onGetNextFeed(String nextId) {
        getNextFeed(mAccessToken, nextId);
    }

    @Override
    public void onRegister(android.support.v4.app.Fragment fragment) {
        if (fragment instanceof IFeedFragmentInteraction) {
            mFeedFragment = (IFeedFragmentInteraction) fragment;
        }
    }

    @Override
    public void onUnregister(android.support.v4.app.Fragment fragment) {
        if (fragment instanceof IFeedFragmentInteraction) {
            mFeedFragment = null;
        }
    }

    //--------------------------- UI ----------------------------------------
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action buttons
        switch (item.getItemId()) {
            case R.id.action_websearch:
                // create intent to perform web search for this planet
                //TODO: поискать хэштег
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* The click listner for ListView in the navigation drawer */
    private static class DrawerItemClickListener implements ListView.OnItemClickListener {
        OnSelectItemListener listener;

        public void setListener(OnSelectItemListener listener) {
            this.listener = listener;
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (listener != null) {
                listener.selectItem(position);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //ImageLoader.getInstance().destroy();
        if (mDrawerClickListener != null) {
            mDrawerClickListener.setListener(null);
        }
        if (mDrawerList != null) {
            mDrawerList.setOnItemClickListener(null);
        }
        if (mDrawerLayout != null) {
            mDrawerLayout.setDrawerListener(null);
        }
        mDrawerToggle = null;
    }
}
