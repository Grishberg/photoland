package com.grishberg.obmenogram.ui.fragments;

import android.app.Activity;
import android.database.Cursor;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.grishberg.obmenogram.R;
import com.grishberg.obmenogram.data.db.AppContentProvider;
import com.grishberg.obmenogram.data.db.DbHelper;
import com.grishberg.obmenogram.framework.Utils;
import com.grishberg.obmenogram.framework.interfaces.IFeedFragmentInteraction;
import com.grishberg.obmenogram.framework.interfaces.IMainActivityInteraction;
import com.grishberg.obmenogram.ui.adapters.FeedAdapter;
import com.grishberg.obmenogram.ui.adapters.InfiniteScrollListener;
import com.grishberg.obmenogram.ui.adapters.PreCachingLayoutManager;

import butterknife.Bind;
import butterknife.ButterKnife;

public class FeedFragment extends BaseFragment implements
        LoaderManager.LoaderCallbacks<Cursor>, FeedAdapter.IOnScrollItemListener
        , SwipeRefreshLayout.OnRefreshListener
        , IFeedFragmentInteraction
        {
    private static final String TAG = FeedFragment.class.getSimpleName();
    private static final String ARG_ACCESS_TOKEN = "argAccessToken";
    private static final String SORT_ORDER = DbHelper.FEED_FEED_ID+" DESC";
    private static final int FEED_LOADER = 0;
    private static final int FEED_PAGE_SIZE = 20;

    @Bind(R.id.refresh)
    SwipeRefreshLayout refresh;
    private String mNextMaxId = null;
    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;
    private FeedAdapter mAdapter;
    private boolean mIsRefreshing;
    private IMainActivityInteraction mListener;
            private InfiniteScrollListener mScrollObserver;

    public static FeedFragment newInstance() {
        FeedFragment fragment = new FeedFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public FeedFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_feed, container, false);
        ButterKnife.bind(this, view);
        mNextMaxId = null;
        if (savedInstanceState == null) {
            getLoaderManager().initLoader(FEED_LOADER, null, this);
        }

        // use a linear layout manager
        PreCachingLayoutManager layoutManager = new PreCachingLayoutManager(getActivity()
                , LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(null);
        Point screenSize = Utils.getScreenSize(getActivity());
        mAdapter = new FeedAdapter(getActivity(), null, screenSize.x, this);
        recyclerView.setAdapter(mAdapter);
        mScrollObserver = new InfiniteScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                mListener.onGetNextFeed(mNextMaxId);
            }
        };
        recyclerView.addOnScrollListener(mScrollObserver);
        refresh = (SwipeRefreshLayout) view.findViewById(R.id.refresh);
        refresh.setOnRefreshListener(this);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof IMainActivityInteraction){
            mListener = (IMainActivityInteraction) activity;
            mListener.onRegister(this);
        }
    }

    //реакция на ответ от сервиса
    @Override
    public void onGetFeedResponse() {
        if (mAdapter != null) {
            getLoaderManager().restartLoader(FEED_LOADER, null, this);
        }
        if (mIsRefreshing) {
            refresh.setRefreshing(false);
            mIsRefreshing = false;
        }
    }

    @Override
    public void onImageLoaded(long id) {
        if(mAdapter != null){
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onScroll(String nextAccessToken, String nextMaxId) {
        mNextMaxId = nextMaxId;
    }

    //---------------------------
    @Override
    public void onRefresh() {
        refresh.setRefreshing(true);
        mIsRefreshing = true;
        mListener.onGetNextFeed(null);
    }

    //--------- cursor ----------
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case FEED_LOADER:
                // Returns a new CursorLoader
                return new CursorLoader(
                        getActivity(),   // Parent activity context
                        AppContentProvider.CONTENT_URI_FEED, // Table to query
                        null,     // Projection to return
                        null,            // No selection clause
                        null,            // No selection arguments
                        SORT_ORDER             // Default sort order
                );
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case FEED_LOADER:
                mAdapter.swapCursor(data);
                if (data != null && data.getCount() == 0) {
                    Log.d(TAG, "cursor data is empty");
                    mListener.onGetNextFeed(null);
                }
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        if (loader.getId() == FEED_LOADER) {
            mAdapter.changeCursor(null);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener.onUnregister(this);
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(mAdapter != null){
            mAdapter.release();
        }
        if(recyclerView != null){
            recyclerView.removeOnScrollListener(mScrollObserver);
        }
        ButterKnife.unbind(this);
        Log.d(TAG, "on destroy fragment");
    }
}
