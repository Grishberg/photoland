package com.grishberg.obmenogram.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;

/**
 * Created by g on 10.08.15.
 */
public class BaseFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

}
