package com.grishberg.obmenogram.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by g on 06.10.15.
 */
public class TriggerButton extends View {
    public TriggerButton(Context context) {
        super(context);
    }

    public TriggerButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
